use serde::ser::Serialize;
use std::ops::AddAssign;

use crate::param_segment::ParamSegment;

pub trait ParamSerializer {
    fn add_value<T>(&mut self, v: T)
    where
        ParamSegment: AddAssign<T>;
    fn push_key(&mut self, v: impl Serialize);
    fn pop_key(&mut self);
    fn start_seq(&mut self);
    fn end_seq(&mut self);
    fn start_map(&mut self);
}
