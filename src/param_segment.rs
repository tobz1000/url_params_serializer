use serde::ser::Serialize;
use std::ops::AddAssign;

use crate::param_serializer_trait::ParamSerializer;

/// A literal string segment of the URL query params - either part of a key, a full key, or a value.
/// Primitive types in the Serde data model, and strings, can be converted into a param segment.
/// These conversions are defined on the `ser::Serializer::serialize_x` implementations for
/// `param_serialize_trait::Wrap<&mut impl ParamSerializer>`.
pub struct ParamSegment(pub Option<String>);

impl ParamSerializer for ParamSegment {
    fn add_value<T>(&mut self, v: T)
    where
        ParamSegment: AddAssign<T>,
    {
        *self += v;
    }

    fn push_key(&mut self, _v: impl Serialize) {
        panic!();
    }

    fn pop_key(&mut self) {
        panic!();
    }

    fn start_seq(&mut self) {
        panic!();
    }

    fn end_seq(&mut self) {
        panic!();
    }

    fn start_map(&mut self) {
        panic!();
    }
}

impl AddAssign<ParamSegment> for ParamSegment {
    fn add_assign(&mut self, add: ParamSegment) {
        match add {
            ParamSegment(Some(s)) => {
                *self += s;
            }
            ParamSegment(None) => {}
        }
    }
}

impl AddAssign<&ParamSegment> for ParamSegment {
    fn add_assign(&mut self, add: &ParamSegment) {
        match add {
            ParamSegment(Some(s)) => {
                *self += &s[..];
            }
            ParamSegment(None) => {}
        }
    }
}

impl AddAssign<String> for ParamSegment {
    fn add_assign(&mut self, add: String) {
        match self {
            ParamSegment(Some(s)) => {
                s.push_str(add.as_str());
            }
            ParamSegment(None) => {
                *self = ParamSegment(Some(add));
            }
        }
    }
}

impl AddAssign<&str> for ParamSegment {
    fn add_assign(&mut self, add: &str) {
        match self {
            ParamSegment(Some(s)) => {
                s.push_str(add);
            }
            ParamSegment(None) => {
                *self = ParamSegment(Some(add.to_string()));
            }
        }
    }
}

impl AddAssign<char> for ParamSegment {
    fn add_assign(&mut self, add: char) {
        match self {
            ParamSegment(Some(s)) => {
                s.push(add);
            }
            ParamSegment(None) => {
                *self = ParamSegment(Some(add.to_string()));
            }
        }
    }
}

impl Into<String> for ParamSegment {
    fn into(self) -> String {
        match self {
            ParamSegment(Some(s)) => s,
            ParamSegment(None) => "".to_owned(),
        }
    }
}
