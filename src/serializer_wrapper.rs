use serde::de::value::Error as SerdeError;
use serde::ser;
use serde::ser::Serialize;

use crate::param_serializer_trait::ParamSerializer;

pub struct Wrap<T>(pub T);

macro_rules! add_scalar_value {
    ($fn_name:ident, $type:ty) => {
        fn $fn_name(self, val: $type) -> Result<Self::Ok, Self::Error> {
            self.0.add_value(val.to_string());
            Ok(())
        }
    }
}

impl<T: ParamSerializer> ser::Serializer for &mut Wrap<T> {
    type Ok = ();
    type Error = SerdeError;

    type SerializeSeq = Self;
    type SerializeTuple = Self::SerializeSeq;
    type SerializeTupleStruct = Self::SerializeSeq;
    type SerializeTupleVariant = Self::SerializeSeq;
    type SerializeMap = Self;
    type SerializeStruct = Self::SerializeMap;
    type SerializeStructVariant = Self::SerializeMap;

    fn serialize_bool(self, v: bool) -> Result<Self::Ok, Self::Error> {
        self.0.add_value(if v { "true" } else { "false" });
        Ok(())
    }

    add_scalar_value!(serialize_i8, i8);
    add_scalar_value!(serialize_i16, i16);
    add_scalar_value!(serialize_i32, i32);
    add_scalar_value!(serialize_i64, i64);
    add_scalar_value!(serialize_i128, i128);
    add_scalar_value!(serialize_u8, u8);
    add_scalar_value!(serialize_u16, u16);
    add_scalar_value!(serialize_u32, u32);
    add_scalar_value!(serialize_u64, u64);
    add_scalar_value!(serialize_u128, u128);
    add_scalar_value!(serialize_f32, f32);
    add_scalar_value!(serialize_f64, f64);
    add_scalar_value!(serialize_char, char);

    fn serialize_str(self, v: &str) -> Result<Self::Ok, Self::Error> {
        self.0.add_value(v);
        Ok(())
    }

    fn serialize_bytes(self, v: &[u8]) -> Result<Self::Ok, Self::Error> {
        use serde::ser::SerializeSeq;
        let mut seq = self.serialize_seq(Some(v.len())).unwrap();
        for byte in v {
            seq.serialize_element(byte).unwrap();
        }
        seq.end().unwrap();

        Ok(())
    }

    fn serialize_unit(self) -> Result<Self::Ok, Self::Error> {
        Ok(())
    }

    fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok, Self::Error> {
        Ok(())
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
    ) -> Result<Self::Ok, Self::Error> {
        self.0.add_value(variant);
        Ok(())
    }

    fn serialize_newtype_struct<U>(
        self,
        _name: &'static str,
        value: &U,
    ) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        value.serialize(self)
    }

    fn serialize_newtype_variant<U>(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
        value: &U,
    ) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        let Wrap(inner) = self;
        inner.push_key(variant);
        value.serialize(&mut *self).unwrap();
        self.0.pop_key();
        Ok(())
    }

    fn serialize_none(self) -> Result<Self::Ok, Self::Error> {
        Ok(())
    }

    fn serialize_some<U>(self, value: &U) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        value.serialize(self)
    }

    fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq, Self::Error> {
        self.0.start_seq();
        Ok(self)
    }

    fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple, Self::Error> {
        self.0.start_seq();
        Ok(self)
    }

    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleStruct, Self::Error> {
        self.0.start_seq();
        Ok(self)
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleVariant, Self::Error> {
        self.0.push_key(variant);
        self.0.start_seq();
        Ok(self)
    }

    fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap, Self::Error> {
        self.0.start_map();
        Ok(self)
    }

    fn serialize_struct(
        self,
        _name: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeStruct, Self::Error> {
        self.0.start_map();
        Ok(self)
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeStructVariant, Self::Error> {
        self.0.push_key(variant);
        self.0.start_map();
        Ok(self)
    }

    fn is_human_readable(&self) -> bool {
        true
    }
}

impl<'a, T: ParamSerializer> ser::SerializeSeq for &mut Wrap<T> {
    type Ok = ();
    type Error = SerdeError;

    fn serialize_element<U>(&mut self, value: &U) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        self.0.end_seq();
        Ok(())
    }
}

impl<'a, T: ParamSerializer> ser::SerializeTuple for &mut Wrap<T> {
    type Ok = ();
    type Error = SerdeError;

    fn serialize_element<U>(&mut self, value: &U) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        self.0.end_seq();
        Ok(())
    }
}

impl<T: ParamSerializer> ser::SerializeTupleStruct for &mut Wrap<T> {
    type Ok = ();
    type Error = SerdeError;

    fn serialize_field<U>(&mut self, value: &U) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        self.0.end_seq();
        Ok(())
    }
}

impl<T: ParamSerializer> ser::SerializeTupleVariant for &mut Wrap<T> {
    type Ok = ();
    type Error = SerdeError;

    fn serialize_field<U>(&mut self, value: &U) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        value.serialize(&mut **self).unwrap();
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        self.0.end_seq();
        self.0.pop_key();
        Ok(())
    }
}

impl<T: ParamSerializer> ser::SerializeMap for &mut Wrap<T> {
    type Ok = ();
    type Error = SerdeError;

    fn serialize_key<U>(&mut self, key: &U) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        self.0.push_key(key);
        Ok(())
    }

    fn serialize_value<U>(&mut self, value: &U) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        value.serialize(&mut **self).unwrap();
        self.0.pop_key();
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        Ok(())
    }
}

impl<T: ParamSerializer> ser::SerializeStruct for &mut Wrap<T> {
    type Ok = ();
    type Error = SerdeError;

    fn serialize_field<U>(&mut self, key: &'static str, value: &U) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        self.0.push_key(key);
        value.serialize(&mut **self).unwrap();
        self.0.pop_key();
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        Ok(())
    }
}

impl<T: ParamSerializer> ser::SerializeStructVariant for &mut Wrap<T> {
    type Ok = ();
    type Error = SerdeError;

    fn serialize_field<U>(&mut self, key: &'static str, value: &U) -> Result<Self::Ok, Self::Error>
    where
        U: ?Sized + Serialize,
    {
        self.0.push_key(key);
        value.serialize(&mut **self).unwrap();
        self.0.pop_key();
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        self.0.pop_key();
        Ok(())
    }
}
