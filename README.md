# `url_params_serializer`

Allows the serialization of types which implement [Serde's](https://serde.rs/) `Serializer` trait
to HTTP GET query parameters. Typical usage:

```rust
use serde_derive::Serialize;
use url::Url;
use url_params_serializer::to_url_params;

#[derive(Serialize)]
struct Params {
    bar: &'static str;
    baz: usize
}

let params = Params { bar: "spam", baz: 5 };
let url = Url::parse_with_params("https://foo.com", to_url_params(params));

assert_eq!(url.into_string(), "https://foo.com/?bar=spam&baz=5");
```

Some more complex types can be serialized too; see the documentation for `to_url_params` for more
information.